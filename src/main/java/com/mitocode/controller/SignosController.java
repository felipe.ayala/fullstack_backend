package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@RestController
@RequestMapping("signos")
public class SignosController {

	@Autowired
	private ISignosService service;

	@GetMapping(produces = "application/json")
	public List<Signos> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Signos listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Signos registrar(@RequestBody Signos Signos) {
		return service.registrar(Signos);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Signos modificar(@RequestBody Signos Signos) {
		return service.modificar(Signos);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
